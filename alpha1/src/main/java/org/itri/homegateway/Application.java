package org.itri.homegateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Mark on 2016/11/23.
 */

@SpringBootApplication
@EnableScheduling
public class Application implements CommandLineRunner {
    private static final int SERVER_PORT = 8880;

    @Autowired
    DeviceRepository deviceRepository;
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    // 3
    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        log.warn("start 12/25");
        Device device1 = deviceRepository.save(new Device("Light Bub", "Main"));
        Device device2 = deviceRepository.save(new Device("Light Bub2", "Main2"));
//        deviceRepository.findAll().forEach(System.out::println);
        zigBee2531Bean.initCode();

    }

}
