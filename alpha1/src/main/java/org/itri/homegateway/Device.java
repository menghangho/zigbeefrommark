package org.itri.homegateway;


import javax.persistence.*;

/**
 * Created by Mark on 2016/12/13.
 */
@Entity
@Table(name = "DEVICE")
public class Device {
    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false)
    private String deviceName;
    @Column(nullable = false)
    private String deviceDescription;

    public Device() {
    }

    public Device(String deviceName, String deviceDescription) {
        this.deviceName = deviceName;
        this.deviceDescription = deviceDescription;
    }

    @Override
    public String toString() {
        return deviceName + "/" + deviceDescription;
    }
}
