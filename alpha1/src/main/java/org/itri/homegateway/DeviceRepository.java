package org.itri.homegateway;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mark on 2016/12/13.
 */
public interface DeviceRepository extends JpaRepository<Device, Integer> {
}
