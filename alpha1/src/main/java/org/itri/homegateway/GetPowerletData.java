package org.itri.homegateway;

import org.bubblecloud.zigbee.v3.CommandResult;
import org.bubblecloud.zigbee.v3.ZigBeeApiDongleImpl;
import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.bubblecloud.zigbee.v3.model.Status;
import org.bubblecloud.zigbee.v3.zcl.field.ReadAttributeStatusRecord;
import org.bubblecloud.zigbee.v3.zcl.protocol.command.general.ReadAttributesResponseCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Mark on 2016/12/17.
 */

//@Component
public class GetPowerletData {
//    @Autowired
//    ZigBee2531Bean zigBee2531Bean;
    private ZigBeeApiDongleImpl mApi;
    private static final Logger log = LoggerFactory.getLogger(GetPowerletData.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public GetPowerletData() {
//        log.info("powerlet data is loaded");
//        mApi = zigBee2531Bean.getApi();
    }

//    @Scheduled(fixedRate = 10000)
    public void reportCurrentData() {
//        mApi = zigBee2531Bean.getApi();
//        log.info("get current data");
//        try {
//            pollPowerPlug();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
    }

    private void pollPowerPlug() throws InterruptedException, ExecutionException {
        int cnt = 0;
        for (ZigBeeDevice d : mApi.getNetworkState().getDevices()) {
            if (!InSnergy.isPowerPlug(d)) {
                log.info("no powerlet yet");
                continue;
            }
            cnt++;
            log.debug("polling device: {}", d);
            Future<CommandResult> future = mApi.read(d, InSnergy.POWER_PLUG_ZB_CLUSTER_ID, InSnergy.POWER_PLUG_ZB_ATTR_ID_ELECTRICITY);
            CommandResult result = future.get();
            if (result == null) {
                log.error("null result skipped");
                continue;
            }
            ReadAttributesResponseCommand response = result.getResponse();
            if (response == null) {
                log.error("null response skipped");
                continue;
            }

            List<ReadAttributeStatusRecord> records = response.getRecords();
            if (records == null) {
                log.error("null records skipped");
                continue;
            }
            ReadAttributeStatusRecord record = records.get(0);
            int statusCode = record.getStatus();
            if (statusCode == 0) {
                String stringVal = (String) record.getAttributeValue();
                InSnergy.ElectricityValue values = InSnergy.parseRawElectricityValue(stringVal.getBytes());
                //               mDb.insert(d, values);
                log.debug("power plug polled value: {}" + values);
            } else {
                final Status status = Status.getStatus((byte) statusCode);
                log.error("Attribute value read error: {}", status);
            }
        }
        if (cnt > 0) {
//            postMessage(MSG_UPDATE_EV);
        } else {
//            postMessage(MSG_DEBUG);
        }
    }

}
