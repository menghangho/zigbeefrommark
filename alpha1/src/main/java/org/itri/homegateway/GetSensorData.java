package org.itri.homegateway;

import org.bubblecloud.zigbee.v3.CommandResult;
import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.bubblecloud.zigbee.v3.model.Status;
import org.bubblecloud.zigbee.v3.zcl.field.AttributeReport;
import org.bubblecloud.zigbee.v3.zcl.field.ReadAttributeStatusRecord;
import org.bubblecloud.zigbee.v3.zcl.protocol.command.general.ReadAttributesResponseCommand;
import org.bubblecloud.zigbee.v3.zcl.protocol.command.general.ReportAttributesCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Mark on 2016/11/30.
 */


@Component
public class GetSensorData {
    @Autowired
    ZigBee2531Bean zigBee2531Bean;

    private static final Logger log = LoggerFactory.getLogger(GetSensorData.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 10000)
    public void reportCurrentData() {
        log.info("Check sensor and write data here..The time is now {}", dateFormat.format(new Date()));
        for (ZigBeeDevice d : zigBee2531Bean.getApi().getDevices()) {
            if (!InSnergy.isPowerPlug(d)) {
               // continue;
            }

            log.debug("polling device: {}", d);
            Future<CommandResult> future = zigBee2531Bean.getApi().read(d, InSnergy.POWER_PLUG_ZB_CLUSTER_ID, InSnergy.POWER_PLUG_ZB_ATTR_ID_ELECTRICITY);
            CommandResult result = null;
            try {
                result = future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if (result == null) {
                log.error("null result skipped");
                continue;
            }
            ReadAttributesResponseCommand response = result.getResponse();
            if (response == null) {
                log.error("null response skipped");
                continue;
            }

            List<ReadAttributeStatusRecord> records = response.getRecords();
            if (records == null) {
                log.error("null records skipped");
                continue;
            }
            ReadAttributeStatusRecord record = records.get(0);
            int statusCode = record.getStatus();
            if (statusCode == 0) {
                String stringVal = (String) record.getAttributeValue();
                InSnergy.ElectricityValue values = InSnergy.parseRawElectricityValue(stringVal.getBytes());

                log.debug("power plug polled value: {}", values);
            } else {
                final Status status = Status.getStatus((byte) statusCode);
                log.error("Attribute value read error: {}", status);
            }

        }
    }
}
