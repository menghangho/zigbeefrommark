package org.itri.homegateway;

import com.google.common.primitives.Ints;
import org.bubblecloud.zigbee.v3.ZigBeeDevice;

import java.nio.ByteBuffer;

import static java.util.Arrays.copyOfRange;

public class InSnergy {
    public static final int POWER_PLUG_ZB_CLUSTER_ID = 0x0702;
    public static final int POWER_PLUG_ZB_ATTR_ID_ELECTRICITY = 0x8000;

    public static class ElectricityValue {
        public final long voltage;
        public final long current;
        public final long frequency;
        public final long powerFactor;
        public final long activePower;
        public final long apparentPower;
        public final long mainEnergy;


        public ElectricityValue(long voltage, long current, long frequency, long powerFactor, long activePower, long apparentPower, long mainEnergy) {
            this.voltage = voltage;
            this.current = current;
            this.frequency = frequency;
            this.powerFactor = powerFactor;
            this.activePower = activePower;
            this.apparentPower = apparentPower;
            this.mainEnergy = mainEnergy;
        }

        @Override
        public String toString() {
            return "ElectricityValue{" +
                    "voltage=" + voltage +
                    ", current=" + current +
                    ", frequency=" + frequency +
                    ", powerFactor=" + powerFactor +
                    ", activePower=" + activePower +
                    ", apparentPower=" + apparentPower +
                    ", mainEnergy=" + mainEnergy +
                    '}';
        }
    }

    private static long toLong(byte value[]) {
        ByteBuffer bb = ByteBuffer.wrap(value);
        long res;
        switch (value.length) {
            case 1:
                res = value[0];
                res &= 0xff;
                return res;
            case 2:
                res = bb.getShort();
                res &= 0xffff;
                return res;
            case 4:
                res = bb.getInt();
                res &= 0xffffffff;
                return res;
            default:
                throw new IllegalArgumentException("unsupported length for conversion: " + value.length);
        }
    }

    private static long toLong(byte value[], int from, int to) {
        return toLong(copyOfRange(value, from, to));
    }

    public static ElectricityValue parseRawElectricityValue(byte value[]) {
        long voltage = toLong(value, 0, 2);
        long current = toLong(value, 2, 4);
        long frequency = toLong(value, 4, 6);
        long powerFactor = toLong(value, 6, 7);
        long activePower = toLong(value, 7, 11);
        long apparentPower = toLong(value, 11, 15);
        // TODO: impl 6 bytes parsing
        long mainEnergy = 0;
        return new ElectricityValue(
                voltage, current, frequency, powerFactor, activePower, apparentPower, mainEnergy
        );
    }

    public static boolean isPowerPlug(ZigBeeDevice dev) {
        return Ints.contains(dev.getInputClusterIds(), POWER_PLUG_ZB_CLUSTER_ID);
    }
}
