package org.itri.homegateway;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mark on 2016/11/23.
 */
@RestController
public class PingController {
    @RequestMapping("/ping")
    public Event helloPing(@RequestParam(value = "name",
            defaultValue = "Gilbert")String name){
        return new Event(1,"Hello World:"+name);
    }
}
