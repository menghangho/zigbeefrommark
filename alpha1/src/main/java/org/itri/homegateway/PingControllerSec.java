package org.itri.homegateway;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mark on 2016/11/23.
 */
@Secured("USER")
@RestController
public class PingControllerSec {
    @Secured("USER")
    @RequestMapping(value="/secure/ping")
    public Event helloPing(@RequestParam(value = "name",
            defaultValue = "Gilbert")String name){
        return new Event(1,"[Secure]:Hello World:"+name);
    }
}
