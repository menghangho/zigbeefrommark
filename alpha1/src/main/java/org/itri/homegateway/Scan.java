package org.itri.homegateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mark on 2016/12/18.
 */
@RestController
public class Scan {
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    @RequestMapping("/scan")
    public Event helloPing(@RequestParam(value = "action",
            defaultValue = "true")String name){
        return new Event(1,"Switch Off OK:"+name+",total="+zigBee2531Bean.getApi().getDevices().size());
    }
}
