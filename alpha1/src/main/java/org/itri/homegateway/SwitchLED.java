package org.itri.homegateway;

import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mark on 2016/12/27.
 */
@RestController
public class SwitchLED {
    private static final Logger log = LoggerFactory.getLogger(SwitchLED.class);
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    @RequestMapping("/switchLED")
    public Event helloPing(@RequestParam(value = "name",
            defaultValue = "Gilbert")String name){
        for (ZigBeeDevice d : zigBee2531Bean.getApi().getDevices()) {

            if (d.getDeviceId()!=0) {
                try {

                    zigBee2531Bean.getApi().on(d);
                    zigBee2531Bean.getApi().color(d, 1.0, 1.0, 1.0, 2.0);
                    Thread.sleep(3000);
                    zigBee2531Bean.getApi().color(d, 1.0, 0.0, 0.0, 2.0);
                    Thread.sleep(3000);
                    zigBee2531Bean.getApi().color(d, 0.0, 1.0, 0.0, 2.0);
                    Thread.sleep(3000);
                    zigBee2531Bean.getApi().color(d, 0.0, 0.0, 1.0, 2.0);
                    Thread.sleep(3000);
                    zigBee2531Bean.getApi().off(d);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            ;

        }
        return new Event(1,"Switch Off OK:"+name+",total="+zigBee2531Bean.getApi().getDevices().size());
    }
}
