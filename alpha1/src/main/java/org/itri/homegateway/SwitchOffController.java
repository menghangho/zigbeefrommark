package org.itri.homegateway;


import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mark on 2016/12/17.
 */
@RestController
public class SwitchOffController {
    private static final Logger log = LoggerFactory.getLogger(SwitchOffController.class);
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    @RequestMapping("/switchOff")
    public Event helloPing(@RequestParam(value = "name",
            defaultValue = "Gilbert")String name){
        for (ZigBeeDevice d : zigBee2531Bean.getApi().getNetworkState().getDevices()) {

            if (d.getDeviceType()==1) {
                log.warn("off device: {}", d);
                log.warn("process:"+d.getDeviceId()+":type="+d.getDeviceType());
                zigBee2531Bean.getApi().off(d);
            }

        }
        return new Event(1,"Switch Off OK:"+name+",total="+zigBee2531Bean.getApi().getDevices().size());
    }
}
