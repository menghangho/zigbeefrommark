package org.itri.homegateway;

import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by Mark on 2016/12/17.
 */
@RestController
public class SwitchOnController {
    private static final Logger log = LoggerFactory.getLogger(SwitchOnController.class);
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    @RequestMapping("/switchOn")
    public Event helloPing(@RequestParam(value = "name",
            defaultValue = "Gilbert")String name){
        for (ZigBeeDevice d : zigBee2531Bean.getApi().getNetworkState().getDevices()) {
            if (d.getDeviceType()==1) {
                log.warn("on device: {}", d.getDeviceId());
                zigBee2531Bean.getApi().on(d);
            }

        }
        return new Event(1,"Switch On OK:"+name);
    }
}
