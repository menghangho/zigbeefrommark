package org.itri.homegateway;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.*;
import org.bubblecloud.zigbee.v3.ZigBeeDevice;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

/**
 * Created by Mark on 2016/12/28.
 */
@SpringUI
@Theme("valo")
public class VaadinUI extends UI {
    @Autowired
    ZigBee2531Bean zigBee2531Bean;
    // we can use either constructor autowiring or field autowiring
    @Autowired
    private SpringViewProvider viewProvider;

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout root = new VerticalLayout();
        for (ZigBeeDevice d : zigBee2531Bean.getApi().getDevices()) {

            if (d.getDeviceId() != 0) {

                HorizontalLayout horizontalLayout = new HorizontalLayout();
                Button label = new Button("addr:" + d.getIeeeAddress());
                Button endPoint = new Button("endPoint:" + d.getEndpoint());
                Button deviceType = new Button("type:" + d.getDeviceType());
                Button inputCluster = new Button("input:" + Arrays.toString(d.getInputClusterIds()));
                Button outputCluster = new Button("output:" + Arrays.toString(d.getOutputClusterIds()));
                Button buttonOn = new Button("ON");
                buttonOn.addClickListener((event) -> {
                    zigBee2531Bean.getApi().on(d);
                });
                Button buttonOff = new Button("OFF");
                buttonOff.addClickListener((event) -> {
                    zigBee2531Bean.getApi().off(d);
                });
                horizontalLayout.addComponent(label);
                horizontalLayout.addComponent(endPoint);
                horizontalLayout.addComponent(deviceType);
                horizontalLayout.addComponent(inputCluster);
                horizontalLayout.addComponent(outputCluster);
                horizontalLayout.addComponent(buttonOn);
                horizontalLayout.addComponent(buttonOff);

                Slider sliderRED = new Slider("RED");
                Slider sliderGREEN = new Slider("GREEN");
                Slider sliderBLUE = new Slider("BLUE");
                sliderRED.addValueChangeListener((event) -> {

                    setLight(d,sliderRED.getValue(),sliderGREEN.getValue(),sliderBLUE.getValue());
                });
                sliderGREEN.addValueChangeListener((event) -> {
                    setLight(d,sliderRED.getValue(),sliderGREEN.getValue(),sliderBLUE.getValue());
                });
                sliderBLUE.addValueChangeListener((event) -> {
                    setLight(d,sliderRED.getValue(),sliderGREEN.getValue(),sliderBLUE.getValue());
                });
                root.addComponent(horizontalLayout);
                root.addComponent(sliderRED);
                root.addComponent(sliderGREEN);
                root.addComponent(sliderBLUE);

            }
        }
        root.setSizeFull();
        root.setMargin(true);
        root.setSpacing(true);
        setContent(root);
        final Panel viewContainer = new Panel();
        viewContainer.setSizeFull();
        root.addComponent(viewContainer);
        root.setExpandRatio(viewContainer, 1.0f);



    }

    private void setLight(ZigBeeDevice d, Double red, Double green, Double blue) {
        zigBee2531Bean.getApi().color(d, red, green, blue, 0.1);
    }


}

