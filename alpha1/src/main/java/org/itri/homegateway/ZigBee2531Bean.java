package org.itri.homegateway;


import org.bubblecloud.zigbee.network.port.SerialPortImpl;
import org.bubblecloud.zigbee.v3.*;
import org.bubblecloud.zigbee.v3.model.Status;
import org.bubblecloud.zigbee.v3.rpc.AccessLevel;
import org.bubblecloud.zigbee.v3.rpc.AuthorizationProvider;
import org.bubblecloud.zigbee.v3.rpc.ZigBeeRpcServer;
import org.bubblecloud.zigbee.v3.zcl.field.ReadAttributeStatusRecord;
import org.bubblecloud.zigbee.v3.zcl.protocol.command.general.ReadAttributesResponseCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Mark on 2016/12/17.
 */
@Component
public class ZigBee2531Bean {
    private static final int SERVER_PORT = 8880;
    @Value("${zigbee.port}")
    private String portName;
    private static final int PAN = 0xffff;
    private static final int CHANNEL = 11;
    private static final byte[] NETWORK_KEY = null;
    private static final Logger log = LoggerFactory.getLogger(ZigBee2531Bean.class);
    private ZigBeeApiDongleImpl mApi;

    public ZigBeeApiDongleImpl getApi() {
        return mApi;
    }

    public ZigBee2531Bean() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                initCode();
//            }
//        }).start();
        //       initCode();

    }

    public void initCode() {
        final byte[] networkKey = null; // Default network key
        final SerialPort port = new SerialPortImpl(portName);
        final ZigBeeDongle dongle = new ZigBeeDongleTiCc2531Impl(port, PAN, CHANNEL, networkKey, false);
        final AuthorizationProvider authorizationProvider = new AuthorizationProvider() {
            @Override
            public AccessLevel getAccessLevel(final String accessToken) {
                return AccessLevel.ADMINISTRATION;
            }
        };
        ZigBeeGateway mGw;
        mGw = new ZigBeeGateway(dongle, false);
        ZigBeeRpcServer mServer;
        try {

            mServer = new ZigBeeRpcServer(mGw, SERVER_PORT, null, null, null, authorizationProvider);
            mServer.start();
        } catch (IOException e) {
            mServer = null;
            e.printStackTrace();
        }
        mApi = mGw.getZigBeeApi();
        log.info("prepare start up");
        try {
            if (mApi.startup()) {
                log.warn("start query devices");
                doPoll();
            } else {
                log.error("start up failed, need re start");
                if (mServer != null) {

                    mServer.stop();
                }
                mApi.shutdown();
                port.close();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.warn("restarting...");
                initCode();
            }
        } catch (RuntimeException spe) {
            log.error("error while start up");
            throw new Error();
        }


    }

    private void doPoll() {


        while (keepScan) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                pollPowerPlug();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean keepScan = true;

    private void pollPowerPlug() throws ExecutionException, InterruptedException {
        int cnt = 0;
        for (ZigBeeDevice d : mApi.getNetworkState().getDevices()) {
            if (!InSnergy.isPowerPlug(d)) {
                continue;
            }
            cnt++;
            log.warn(cnt + "polling device: {}", d);
            d.setLabel("My Device1");

            try {
                Future<CommandResult> future = mApi.read(d, InSnergy.POWER_PLUG_ZB_CLUSTER_ID, InSnergy.POWER_PLUG_ZB_ATTR_ID_ELECTRICITY);

                CommandResult result = future.get();
                if (result == null) {
                    log.error("null result skipped");
                    continue;
                }
                ReadAttributesResponseCommand response = result.getResponse();
                if (response == null) {
                    log.error("null response skipped");
                    continue;
                }
                List<ReadAttributeStatusRecord> records = response.getRecords();
                if (records == null) {
                    log.error("null records skipped");
                    continue;
                }
                log.warn("try to read records");
                ReadAttributeStatusRecord record = records.get(0);
                int statusCode = record.getStatus();
                if (statusCode == 0) {
                    String stringVal = (String) record.getAttributeValue();
                    InSnergy.ElectricityValue values = InSnergy.parseRawElectricityValue(stringVal.getBytes());
                    //mDb.insert(d, values);
                    log.warn("power plug polled value: {}", values);
                } else {
                    final Status status = Status.getStatus((byte) statusCode);
                    log.error("Attribute value read error: {}", status);
                }

            } catch (NullPointerException npe) {
                npe.printStackTrace();
                log.error("null pointer exception while read");
            }


        }
        if (cnt > 0) {
            log.debug("finally get 1");
        } else {
            log.debug("get nothing this term");
        }
    }

}
